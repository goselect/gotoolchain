package gotoolchain

import (
	"os"
	"path/filepath"
)

const (
	toolchainsDir = "toolchains"
	downloadsDir  = "downloads"
	versionFile   = "current"
)

// Config contains configuration values.
type Config struct {
	RootDir      string
	ToolchainDir string
	DownloadDir  string
	VersionFile  string
} //func

// NewConfig creates a new Config. It accepts a root and derives the toolchains
// and downloads at root/toolchains and root/downloads respectively.
func NewConfig(root string) *Config {
	return &Config{
		RootDir:      root,
		ToolchainDir: filepath.Join(root, toolchainsDir),
		DownloadDir:  filepath.Join(root, downloadsDir),
		VersionFile:  filepath.Join(root, versionFile),
	}
} //func

// DefaultConfig returns a Config with the default location provided. It assumes
// the user as a home directory. If none is available this function will panic.
func DefaultConfig() *Config {
	home, err := os.UserHomeDir()
	if err != nil {
		panic(err)
	} //if

	return NewConfig(filepath.Join(home, ".goselect"))
} //func

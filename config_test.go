package gotoolchain

import (
	"path/filepath"
	"testing"
)

func TestNewConfig(t *testing.T) {
	root := filepath.Join("toolchains", "root")

	c := NewConfig(root)

	if expected, actual := root, c.RootDir; actual != expected {
		t.Errorf(`expected root dir "%s" but was "%s"`, expected, actual)
	} //if

	if expected, actual := filepath.Join(root, toolchainsDir), c.ToolchainDir; actual != expected {
		t.Errorf(`expected toolchains dir "%s" but was "%s"`, expected, actual)
	} //if

	if expected, actual := filepath.Join(root, downloadsDir), c.DownloadDir; actual != expected {
		t.Errorf(`expected downloads dir "%s" but was "%s"`, expected, actual)
	} //if
} //func

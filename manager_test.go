package gotoolchain

import (
	"io/fs"
	"os"
	"path/filepath"
	"strings"
	"testing"
	"testing/fstest"
)

func TestNewManager(t *testing.T) {
	t.Run("Config", func(t *testing.T) {
		var cfg Config

		m := NewManager(&cfg)

		if m.cfg != &cfg {
			t.Error("expected Manager cfg to be set")
		} //if
	}) //func
	t.Run("NilConfig", func(t *testing.T) {
		m := NewManager(nil)

		if m.cfg == nil {
			t.Error("expected Manager cfg to be set")
		} //if
	}) //func
} //func

func Test_listVersions(t *testing.T) {
	t.Run("Bad", func(t *testing.T) {
		badname := "not a version"
		files := fstest.MapFS{
			badname: &fstest.MapFile{
				Mode: fs.ModeDir,
			},
		}

		_, err := listVersions(files)
		if err == nil {
			t.Fatal("expected error for bad version string")
		} //if

		if !strings.Contains(err.Error(), badname) {
			t.Errorf(`expected error string "%s" to contain "%s"`, err.Error(),
				badname)
		} //if
	}) //func
	t.Run("Good", func(t *testing.T) {
		files := fstest.MapFS{
			"1.17": &fstest.MapFile{
				Mode: fs.ModeDir,
			},
			"go1.17.5": &fstest.MapFile{
				Mode: fs.ModeDir,
			},
			"1.18beta2": &fstest.MapFile{
				Mode: fs.ModeDir,
			},
		}

		versions, err := listVersions(files)
		if err != nil {
			t.Fatal("unexpected error:", err)
		} //if

		if expected, actual := 3, len(versions); actual != expected {
			t.Errorf("expected number of versions to be %d but was %d",
				expected, actual)
		} //if

		checkOrder := func(i int, expected string) {
			if actual := versions[i].String(); actual != expected {
				t.Errorf(`expected version at %d to be "%s" but was "%s"`, i,
					expected, actual)
			} //if
		} //func

		checkOrder(0, "1.17")
		checkOrder(1, "1.17.5")
		checkOrder(2, "1.18beta2")
	}) //func
} //func

func TestManagerListVersions(t *testing.T) {
	tmpdir := t.TempDir()
	tcdir := filepath.Join(tmpdir, toolchainsDir)
	verstr := "1.17.7"

	err := os.MkdirAll(filepath.Join(tcdir, verstr), 0755)
	if err != nil {
		t.Fatal("unexpected error:", err)
	} //if

	cfg := Config{
		ToolchainDir: tcdir,
	}

	mgr := Manager{
		cfg: &cfg,
	}

	versions, err := mgr.ListVersions()
	if err != nil {
		t.Fatal("unexpected error:", err)
	} //if

	if expected, actual := 1, len(versions); actual != expected {
		t.Errorf(`expected num of versions to be %d but was %d`, expected,
			actual)
	} //if

	if expected, actual := verstr, versions[0].String(); actual != expected {
		t.Errorf(`expected version "%s" but was "%s"`, expected, actual)
	} //if
} //func

func Test_currentVersion(t *testing.T) {
	version := Version{
		Major: 1,
		Minor: 17,
		Patch: 6,
	}

	files := fstest.MapFS{
		versionFile: &fstest.MapFile{
			Data: []byte(version.String()),
		},
	}

	v, err := getCurrentVersion(files, versionFile)
	if err != nil {
		t.Fatal("unexpected error:", err)
	} //if

	if expected, actual := version.String(), v.String(); actual != expected {
		t.Errorf(`expected version "%s" but was "%s`, expected, actual)
	} //if
} //func

func TestManagerCurrentVersion(t *testing.T) {
	tmpdir := t.TempDir()
	verstr := "1.17.7"
	verfile := filepath.Join(tmpdir, versionFile)

	err := os.WriteFile(verfile, []byte(verstr), 0644)
	if err != nil {
		t.Error("unexpected error:", err)
	} //if

	cfg := Config{
		VersionFile: verfile,
	}

	mgr := Manager{
		cfg: &cfg,
	}

	ver, err := mgr.GetCurrentVersion()
	if err != nil {
		t.Error("unexpected error:", err)
	} //if

	if expected, actual := verstr, ver.String(); actual != expected {
		t.Errorf(`expected version "%s" but was "%s"`, expected, actual)
	} //if
} //func

func Test_checkForVersion(t *testing.T) {
	vers := []*Version{
		{
			Major: 1,
			Minor: 17,
			Patch: -1,
		},
		{
			Major: 1,
			Minor: 17,
			Patch: 7,
		},
		{
			Major:    1,
			Minor:    18,
			Modifier: "beta2",
		},
	}

	t.Run("Present", func(t *testing.T) {
		for _, v := range vers {
			if !checkForVersion(v, vers) {
				t.Errorf("expected %v to be found", v)
			} //if
		} //for
	}) //func
	t.Run("Absent", func(t *testing.T) {
		others := []*Version{
			{
				Major: 1,
				Minor: 16,
				Patch: 16,
			},
			{
				Major: 1,
				Minor: 17,
				Patch: 2,
			},
		}

		for _, v := range others {
			if checkForVersion(v, vers) {
				t.Errorf("expected %v to not be found", v)
			} //if
		} //for
	}) //func
} //func

func TestManagerContains(t *testing.T) {
	tmpdir := t.TempDir()
	tcdir := filepath.Join(tmpdir, toolchainsDir)
	v := Version{
		Major: 1,
		Minor: 17,
		Patch: 7,
	}
	verstr := v.String()

	err := os.MkdirAll(filepath.Join(tcdir, verstr), 0755)
	if err != nil {
		t.Fatal("unexpected error:", err)
	} //if

	cfg := Config{
		ToolchainDir: tcdir,
	}

	mgr := Manager{
		cfg: &cfg,
	}

	found, err := mgr.Contains(&v)
	if err != nil {
		t.Fatal("unexpected error:", err)
	} //if

	if !found {
		t.Error("expected to find", v)
	} //if

	v.Patch = v.Patch - 1

	found, err = mgr.Contains(&v)
	if err != nil {
		t.Fatal("unexpected error:", err)
	} //if

	if found {
		t.Error("expected to not find", v)
	} //if
} //func

func Test_setCurrentVersionFile(t *testing.T) {
	versionFile := filepath.Join(t.TempDir(), versionFile)

	defer func() {
		os.Remove(versionFile)
	}() //func

	version := Version{
		Major: 1,
		Minor: 17,
		Patch: 6,
	}

	err := setCurrentVersionFile(versionFile, &version)
	if err != nil {
		t.Fatal("unexpected error:", err)
	} //if
} //func

func TestManagerSetVersion(t *testing.T) {
	tmpdir := t.TempDir()
	verfile := filepath.Join(tmpdir, versionFile)

	cfg := Config{
		VersionFile: verfile,
	}

	mgr := Manager{
		cfg: &cfg,
	}

	ver := Version{
		Major: 1,
		Minor: 17,
		Patch: 7,
	}

	err := mgr.SetCurrentVersion(&ver)
	if err != nil {
		t.Fatal("unexpected error:", err)
	} //if

	data, err := os.ReadFile(verfile)
	if err != nil {
		t.Fatal("unexpected error:", err)
	} //if

	if expected, actual := ver.String(), string(data); actual != expected {
		t.Errorf(`expected version "%s" but was "%s"`, expected, actual)
	} //if
} //func

func TestManagerInstallToolchainFS(t *testing.T) {
	tmpdir := t.TempDir()

	mgr := Manager{
		cfg: &Config{
			ToolchainDir: tmpdir,
		},
	}

	v := Version{
		Major: 1,
		Minor: 17,
		Patch: 7,
	}

	toolchainFS := fstest.MapFS{
		"go": &fstest.MapFile{
			Mode: fs.ModeDir,
		},
		filepath.Join("go", "bin"): &fstest.MapFile{
			Mode: fs.ModeDir,
		},
		filepath.Join("go", "bin", "go"): &fstest.MapFile{
			Mode: 0755,
			Data: []byte{1},
		},
	}

	err := mgr.InstallToolchainFS(toolchainFS, &v)
	if err != nil {
		t.Fatal("unexpected error:", err)
	} //if

	testdir := func(name string) {
		info, err := os.Stat(name)
		if err != nil {
			t.Fatal("unexpected error:", err)
		} //if

		if !info.IsDir() {
			t.Error("expected dir but was file", name)
		} //if
	} //func

	testfile := func(name string, mode fs.FileMode) {
		info, err := os.Stat(name)
		if err != nil {
			t.Fatal("unexpected error:", err)
		} //if

		if info.IsDir() {
			t.Error("expected file but was dir", name)
		} //if

		if expected, actual := mode, info.Mode(); actual !=
			fs.FileMode(expected) {
			t.Errorf("expected mode %d but was %d for %s", expected, actual,
				name)
		} //if
	} //func

	testdir(filepath.Join(tmpdir, v.String(), "bin"))
	testfile(filepath.Join(tmpdir, v.String(), "bin", "go"), 0755)
} //func

func TestManagerRemove(t *testing.T) {
	tmpdir := t.TempDir()

	v := Version{
		Major: 1,
		Minor: 17,
		Patch: 7,
	}

	vstr := v.String()

	if err := os.Mkdir(filepath.Join(tmpdir, "1.17.6"), 0755); err != nil {
		t.Fatal("unexpected error:", err)
	} //if
	if err := os.Mkdir(filepath.Join(tmpdir, vstr), 0755); err != nil {
		t.Fatal("unexpected error:", err)
	} //if

	mgr := Manager{
		cfg: &Config{
			ToolchainDir: tmpdir,
		},
	}

	mgr.Remove(&v)

	dirfs := os.DirFS(tmpdir)

	if dir := "1.17.6"; fstest.TestFS(dirfs, dir) != nil {
		t.Errorf(`expected dir "%s" to exist`, dir)
	} //if

	if dir := vstr; fstest.TestFS(dirfs, dir) == nil {
		t.Errorf(`expected dir "%s" to not exist`, dir)
	} //if
} //func

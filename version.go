package gotoolchain

import "gitlab.com/goselect/gosem"

// Version type alias to gosem.Version.
//
// Deprecated: This will be removed in the future. Use
// gitlab.com/goselect/gosem.Version instead.
type Version = gosem.Version

module gitlab.com/goselect/gotoolchain

go 1.22

retract v0.4.0

require gitlab.com/goselect/gosem v0.1.1 // indirect

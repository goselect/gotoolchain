package gotoolchain

import (
	"fmt"
	"io"
	"io/fs"
	"os"
	"path/filepath"
	"sort"
	"strings"

	"gitlab.com/goselect/gosem"
)

// Manager queries and modifies a Go toolchains directory.
type Manager struct {
	cfg *Config
} //struct

// NewManager creates a new Manager. If the provided Config is nil then
// DefaultConfig() is called and used.nil
func NewManager(cfg *Config) *Manager {
	if cfg == nil {
		cfg = DefaultConfig()
	} //if

	return &Manager{
		cfg: cfg,
	}
} //func

// listVersions will list the versions found in a FS directory.
func listVersions(dir fs.FS) (versions []*Version, err error) {
	defer func() {
		if err != nil {
			err = fmt.Errorf("list versions: %w", err)
		} //if
	}() //func

	entries, err := fs.ReadDir(dir, ".")
	if err != nil {
		return
	} //if

	versions = make([]*Version, 0, len(entries))

	for _, entry := range entries {
		if !entry.IsDir() {
			continue
		} //if

		name := entry.Name()

		if strings.HasPrefix(name, "go") {
			name = name[2:]
		} //if

		var version *Version
		version, err = gosem.ParseVersion(name)
		if err != nil {
			err = fmt.Errorf("%w (%s)", err, name)
			return
		} //if
		versions = append(versions, version)
	} //for

	sort.Slice(versions, func(i, j int) bool {
		return versions[i].Less(versions[j])
	}) //func

	return
} //func

// ListVersions will list all the Versions.
func (m *Manager) ListVersions() ([]*Version, error) {
	return listVersions(os.DirFS(m.cfg.ToolchainDir))
} //func

// getCurrentVersion will read the current Version from a file in the dir FS.
func getCurrentVersion(dir fs.FS, file string) (version *Version, err error) {
	defer func() {
		if err != nil {
			err = fmt.Errorf("current version: %w", err)
		} //if
	}() // func

	var f fs.File
	f, err = dir.Open(file)
	if err != nil {
		return
	} //if
	defer f.Close()

	var data []byte
	data, err = io.ReadAll(f)
	if err != nil {
		return
	} //if

	version, err = gosem.ParseVersion(string(data))

	return
} //func

// GetCurrentVersion reads the current Version from the current version file.
func (m *Manager) GetCurrentVersion() (*Version, error) {
	return getCurrentVersion(os.DirFS(filepath.Dir(m.cfg.VersionFile)),
		filepath.Base(m.cfg.VersionFile))
} //func

// checkForVersion will check if a specific Version is in a slice of Versions.
func checkForVersion(v *Version, vers []*Version) bool {
	for _, ver := range vers {
		if *ver == *v {
			return true
		} //if
	} //for

	return false
} //func

// Contains will check if a Go toolchain for the provided Version exists.
func (m *Manager) Contains(v *Version) (found bool, err error) {
	defer func() {
		if err != nil {
			err = fmt.Errorf("container: %w", err)
		} //if
	}() //func

	vers, err := m.ListVersions()
	if err != nil {
		return
	} //if

	found = checkForVersion(v, vers)

	return
} //func

// setCurrentVersionFile will write the provided Version to the file.
func setCurrentVersionFile(file string, v *Version) (err error) {
	defer func() {
		if err != nil {
			err = fmt.Errorf("set version: %w", err)
		} //if
	}() //func

	err = os.WriteFile(file, []byte(v.String()), 0644)

	return
} //func

// SetCurrentVerion will set the current version to the provided Version.
func (m *Manager) SetCurrentVersion(v *Version) error {
	return setCurrentVersionFile(m.cfg.VersionFile, v)
} //func

// InstallToolchainFS will install a Go toolchain from the provided toolchain.
// It will install it useing the provided Version so that multiple toolchain
// verions may be installed simultaneously.
func (m *Manager) InstallToolchainFS(toolchain fs.FS, v *Version) (err error) {
	defer func() {
		if err != nil {
			err = fmt.Errorf("install toolchain fs: %w", err)
		} //if
	}() //func

	root := filepath.Join(m.cfg.ToolchainDir, v.String())

	err = fs.WalkDir(toolchain, ".",
		func(path string, d fs.DirEntry, err error) error {
			if err != nil {
				panic(err)
			} //if

			if path == "." || path == "go" {
				return nil
			} //if

			name := filepath.Join(root, path[3:])

			if d.IsDir() {
				return os.MkdirAll(name, 0755)
			} else {
				err = os.MkdirAll(filepath.Dir(name), 0755)
				if err != nil {
					return err
				} //if
			} //if

			in, err := toolchain.Open(path)
			if err != nil {
				return nil
			} //if
			defer in.Close()

			instat, err := in.Stat()
			if err != nil {
				return nil
			} //if

			out, err := os.OpenFile(name, os.O_CREATE|os.O_RDWR, instat.Mode())
			if err != nil {
				return err
			} //if
			defer out.Close()

			_, err = io.Copy(out, in)
			return err
		})

	return
} //func

// Remove a Version from the toolchains directory.
func (m *Manager) Remove(v *Version) {
	vstr := v.String()
	os.RemoveAll(filepath.Join(m.cfg.ToolchainDir, vstr))
} //func

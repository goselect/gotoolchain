# gotoolchain

This modules is used to manage Go toolchain versions.

## Copyright

This is copyrighted and made freely available under the terms of the MIT
license.
